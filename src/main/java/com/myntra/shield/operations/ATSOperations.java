package com.myntra.shield.operations;

import java.io.UnsupportedEncodingException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.ats.blocked.client.ATSClient;
import com.myntra.ats.user.client.request.UserDetailsRequest;
import com.myntra.ats.blocked.client.response.UserDetailsResponse;
import com.myntra.idea.client.IdeaClient;
import com.myntra.idea.response.ProfileResponse;

@Component
public class ATSOperations {
	
	ATSClient atsClient;
	
	IdeaClient ideaClient;
	
	@Value("${ats.url}")
	String atsUrl="http://atscluster-ats.dockins.myntra.com/myntra-ats-service/";
	
	@Value("${ats.username}")
	String username="myusername";
	
	@Value("${ats.password}")
	String password="mypassword";
	
	@Value("${idea.url}")
	String ideaUrl="http://atscluster-idea.dockins.myntra.com";
	
	
	
	
	@SuppressWarnings("static-access")
	//@PostConstruct
	public ATSOperations(){
		atsClient = new ATSClient();
		atsClient.setTimeout(5000);
		atsClient.setBaseUrl(atsUrl);
		atsClient.setAuthUser(username);
		atsClient.setAuthPassword(password);
		
		ideaClient = new IdeaClient(ideaUrl);
		
	}
	
	
	
	public UserDetailsResponse getUserDetails(String login) {
		UserDetailsResponse userDetails = ATSClient.getUserDetails(login);
		return userDetails;
		
	}

	public void markReturnAbuser(String email) throws UnsupportedEncodingException {
		ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
		String uidx = profileResponse.getEntry().getUidx();
		
		UserDetailsRequest request = new UserDetailsRequest();
		request.setLogin(uidx);
		request.setIsReturnAbuser(true);
		request.setReasonReturnAbuser("LINKED_BY_PHONE");
		
		atsClient.setReturnAbuser(request);
		
		
		
	}

	public void changeMaxCod(String email, String label) throws UnsupportedEncodingException {
		UserDetailsRequest detailsRequest = new UserDetailsRequest();
		ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
		String uidx = profileResponse.getEntry().getUidx();
		detailsRequest.setLogin(uidx);
		detailsRequest.setMaxCOD(label);
		detailsRequest.setReasonRTOAbuser("COD108");
		
		atsClient.setCODLimit(detailsRequest);
		
	}

	public void unmarkReturnAbuser(String email) throws UnsupportedEncodingException {
		ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
		String uidx = profileResponse.getEntry().getUidx();
		
		UserDetailsRequest request = new UserDetailsRequest();
		request.setLogin(uidx);
		request.setIsReturnAbuser(false);
		request.setReasonReturnAbuser("DEFAULT");
		
		atsClient.setReturnAbuser(request);
		
	}

}
