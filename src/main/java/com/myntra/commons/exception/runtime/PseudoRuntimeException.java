/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.myntra.commons.exception.runtime;

import com.myntra.commons.codes.StatusCodes;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.codes.StatusResponse.Type;

public class PseudoRuntimeException extends AbstractRuntimeException {
	private static final long serialVersionUID = -8909313964287396246L;

	public PseudoRuntimeException(StatusCodes err, Throwable t) {
		super(err, t);
	}

	public PseudoRuntimeException(StatusCodes err) {
		super(err);
	}

	public PseudoRuntimeException(String message, int code, Throwable cause, StatusResponse.Type type) {
		super(message, code, cause, type);
	}

	public PseudoRuntimeException(String message, int code, Throwable cause) {
		super(message, code, cause);
	}

	public PseudoRuntimeException(String message, int code) {
		super(message, code);
	}
}