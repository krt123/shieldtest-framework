package com.myntra.shield.operations;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myntra.idea.client.IdeaClient;
import com.myntra.idea.client.exception.IDEAClientException;
import com.myntra.idea.entry.UserEntry;
import com.myntra.idea.entry.UserRegistrationEntry;
import com.myntra.idea.response.ProfileResponse;
import com.myntra.idea.response.RegistrationResponse;
import com.myntra.shield.utils.Utils;

@Component
public class IdeaOperations {
	
	@Value("${signup.mobile}")
	String signupMobile="9911539011";
	
	@Value("${idea.url}")
	String ideaUrl="http://atscluster-idea.dockins.myntra.com";
	
//	IdeaClient ideaClient = new IdeaClient(ideaUrl);
	IdeaClient ideaClient;
	
	//@PostConstruct
	public  IdeaOperations(){
		ideaClient = new IdeaClient(ideaUrl);
		
	}

	public String signup(String email) {
		UserRegistrationEntry userRegistrationEntry = getGenericUserRegistrationEntry();
		userRegistrationEntry.setEmail(email);
		userRegistrationEntry.setPhoneNumber(signupMobile);
		
		RegistrationResponse userSignupResponse = ideaClient.signupPhone(userRegistrationEntry);
		
		return userSignupResponse.getEntry().getUidx();
		
	}

	private UserRegistrationEntry getGenericUserRegistrationEntry() {
		// TODO Auto-generated method stub
		UserRegistrationEntry userRegistrationEntry = new UserRegistrationEntry();
		userRegistrationEntry.setAppName("myntra");
		userRegistrationEntry.setVerified(false);
		userRegistrationEntry.setChannel("EMAIL");
		userRegistrationEntry.setGender("MALE");
		userRegistrationEntry.setUserType("REGULAR");
		userRegistrationEntry.setNew_(false);
		userRegistrationEntry.setAccessKey("myntra");
		userRegistrationEntry.setAppBasedRegistration(true);
		return userRegistrationEntry;
	}

	public String getUserStatus(String uidx) throws IOException {
		
//		String response = Utils.getRequest(ideaUrl + "/idea/opt/profile/uidx/myntra/"+ uidx);
//		GsonBuilder gsonBuilder = new GsonBuilder();
//		Gson gson = gsonBuilder.create();
//		RegistrationResponse userResponse = (RegistrationResponse) gson.fromJson(response.toString(), RegistrationResponse.class);
		
		
		ProfileResponse profileResponse = ideaClient.getByUidx("myntra", uidx);
		//return profileResponse;
		return profileResponse.getEntry().getStatus();
		
		
	}

	public String blacklist(String email) throws JSONException, IOException {
		// TODO Auto-generated method stub
		ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
		
		UserEntry userEntry = new UserEntry();
		userEntry.setStatus("BLACKLISTED");
		userEntry.setAppName("myntra");
		userEntry.setUidx(profileResponse.getEntry().getUidx());
		
		changeUserStatus(userEntry, "BLACKLISTED");
		//profileResponse =  ideaClient.updateUser(userEntry);
		
		profileResponse = ideaClient.getByEmail("myntra", email);
		return profileResponse.getEntry().getStatus();
		
	}

	private void changeUserStatus(UserEntry userEntry, String status) throws JSONException, IOException {
		// TODO Auto-generated method stub
		
		StringBuilder url = new StringBuilder();
		url.append(ideaUrl);
		url.append("/idea/opt/profile/changeUserStatus");
		JSONObject jsonParam = new JSONObject();
		
		jsonParam.put("appName", "myntra");
		jsonParam.put("status", status);
		jsonParam.put("uidx", userEntry.getUidx());
		
		String response = putRequest(url.toString(), jsonParam.toString());
//		GsonBuilder gsonBuilder = new GsonBuilder();
//		Gson gson = gsonBuilder.create();
//		ProfileResponse profileResponse = gson.fromJson(response.toString(), ProfileResponse.class);
//		
//		return profileResponse;
	}

	
	public String activateUser(String email) throws JSONException, IOException {
		// TODO Auto-generated method stub
//		ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
//		
//		UserEntry userEntry = new UserEntry();
//		userEntry.setStatus("ACTIVE");
//		userEntry.setAppName("myntra");
//		userEntry.setUidx(profileResponse.getEntry().getUidx());
//		ideaClient.updateUser(userEntry);
//		
//		profileResponse = ideaClient.getByEmail("myntra", email);
//		return profileResponse.getEntry().getStatus();
		ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
		
		UserEntry userEntry = new UserEntry();
		userEntry.setStatus("BLACKLISTED");
		userEntry.setAppName("myntra");
		userEntry.setUidx(profileResponse.getEntry().getUidx());
		
		changeUserStatus(userEntry, "ACTIVE");
		//profileResponse =  ideaClient.updateUser(userEntry);
		
		profileResponse = ideaClient.getByEmail("myntra", email);
		return profileResponse.getEntry().getStatus();
	}

	public ProfileResponse getUserProfile(String uidx) {
		// TODO Auto-generated method stub
		ProfileResponse profileResponse = ideaClient.getByUidx("myntra", uidx);
		return profileResponse;
		
	}
	
	
	public String putRequest(String url, String urlParam) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setDoOutput( true );
		con.setInstanceFollowRedirects( false );
		con.setRequestMethod("PUT");

		//add request header
		con.setRequestProperty("Content-Type", "application/json");
		
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParam);
		wr.flush();
		wr.close();
		
		int responseCode = con.getResponseCode();
		if(responseCode!= 200)
			return null;

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		return response.toString();

	}

}
