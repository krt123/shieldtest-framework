package com.myntra.shield.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.springframework.stereotype.Component;

@Component
public class AssertOperations {

	public void assertActiveState(String userStatus) {
		assertEquals(userStatus, "ACTIVE");
		
	}
	
	public void assertBlacklistedState(String userStatus) {
		assertEquals(userStatus, "BLACKLISTED");
		
	}

	public void assertNonReturnAbuser(Boolean isReturnAbuser) {
		assertFalse(isReturnAbuser);
		
	}
	
	
	public void assertReturnAbuser(Boolean isReturnAbuser) {
		assertTrue(isReturnAbuser);
		
	}

	public void assertMinCod(Double actual, Double expected) {
		assertEquals(actual, expected);
		
	}

	public void assertMaxCod(Double actual, Double expected) {
		assertEquals(actual, expected);
		
	}

}
