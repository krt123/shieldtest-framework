/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.myntra.commons.response;

import com.myntra.commons.codes.StatusResponse;
import java.io.Serializable;

public abstract class AbstractResponse implements Serializable {
	private StatusResponse status;

	public AbstractResponse() {
	}

	public AbstractResponse(StatusResponse status) {
		this.status = status;
	}

	public StatusResponse getStatus() {
		return this.status;
	}

	public void setStatus(StatusResponse status) {
		this.status = status;
	}

	public String toString() {
		return "AbstractResponse [status=" + this.status + "]";
	}
}