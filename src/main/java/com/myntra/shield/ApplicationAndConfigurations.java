package com.myntra.shield;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.myntra.shield.operations.ATSOperations;
import com.myntra.shield.operations.IdeaOperations;

@SpringBootApplication
public class ApplicationAndConfigurations {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ApplicationAndConfigurations.class, args);
	}
	


}
